// ignore_for_file: avoid_void_async

import 'package:dio/dio.dart';
import 'package:pokeapp/models/pokemon.dart';

class PokeRepo {
  final String url = 'https://pokeapi.co/api/v2/';

  void getHttp() async {
    try {
      final response = await Dio().get<String>(url);
      print(response);
    } catch (e) {
      print(e);
    }
  }

  Future<Pokemon> getPokemon(int id) async {
    var pokemon = <String, dynamic>{};

    try {
      final response =
          await Dio().get<Map<String, dynamic>>('$url/pokemon/$id');
      pokemon = response.data!;
    } catch (e) {
      print(e);
    }
    return Pokemon.fromJson(pokemon);
  }
}
